# Project Creation

This project is to create a fake subscription for users to register and be sent to a confirmation
page.

I've went with the MVVM architecture to build this app. Some of the key features this app utilizes:

- Model View ViewModel
- LiveData
- Room Database
- Coroutines
- Data binding
- Regex

## Authors

- [@dnuno](https://www.linkedin.com/in/davidnuno1/)

## Features

- Light/dark mode toggle
- Easily subscribe to your fake subscription!


package com.davidnuno.profilecreation.viewModel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.davidnuno.profilecreation.data.UserRepository

class SignUpViewModelFactory(
    private val repository: UserRepository,
    private val application: Application
) : ViewModelProvider.Factory {

    @Suppress("Unchecked_cast")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(UserRegisterViewModel::class.java)) {

            return UserRegisterViewModel(repository, application) as T
        }
        throw IllegalArgumentException("Not a known ViewModel Class")
    }
}
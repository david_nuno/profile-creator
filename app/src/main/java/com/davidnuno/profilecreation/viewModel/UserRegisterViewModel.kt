package com.davidnuno.profilecreation.viewModel

import android.app.Application
import android.text.TextUtils
import android.widget.Toast
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.davidnuno.profilecreation.data.UserEntity
import com.davidnuno.profilecreation.data.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class UserRegisterViewModel(private val repository: UserRepository, application: Application) :
    AndroidViewModel(application),
    Observable {

    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    var ldUser = MutableLiveData<Array<UserEntity>>()

    @Bindable
    val inputFirstName = MutableLiveData<String>()

    @Bindable
    val inputEmailAddress = MutableLiveData<String>()

    @Bindable
    val inputPassword = MutableLiveData<String>()

    @Bindable
    val inputWebsite = MutableLiveData<String>()

    private val _navigateTo = MutableLiveData<UserEntity>()

    val navigateActivity: MutableLiveData<UserEntity> get() = _navigateTo


    private fun insert(user: UserEntity): Job = viewModelScope.launch {
        repository.insert(user)
    }

    fun submit() {

        if (TextUtils.isEmpty(inputEmailAddress.value) || TextUtils.isEmpty(inputPassword.value)) {

            Toast.makeText(
                getApplication(),
                "Please complete all fields to submit",
                Toast.LENGTH_SHORT
            ).show()
        } else {

            if (isValidEmail(inputEmailAddress.value!!)) {

                uiScope.launch {

                    val users = repository.getEmail(inputEmailAddress.value!!.trim())

                    if (users == null) {

                        val firstName = inputFirstName.value
                        val email = inputEmailAddress.value!!
                        val password = inputPassword.value!!.trim()
                        val website = inputWebsite.value

                        val newUser = UserEntity(0, firstName, email, website, password)

                        insert(newUser)

                        _navigateTo.value = newUser
                        clearEditTextFields()

                    } else {
                        Toast.makeText(
                            getApplication(),
                            "Account already exists",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            } else Toast.makeText(getApplication(), "Invalid email address", Toast.LENGTH_SHORT)
                .show()
        }

    }

    private fun clearEditTextFields() {

        inputFirstName.value = ""
        inputEmailAddress.value = ""
        inputPassword.value = ""
        inputWebsite.value = ""
    }

    private fun isValidEmail(email: String): Boolean {

        var emailPatternRegEx = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+".toRegex()

        return email.trim().matches(emailPatternRegEx)
    }

    fun finishedNavigating() {

        navigateActivity.value = null
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {
    }
}
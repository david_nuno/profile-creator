package com.davidnuno.profilecreation

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.davidnuno.profilecreation.data.UserDatabase
import com.davidnuno.profilecreation.data.UserRepository
import com.davidnuno.profilecreation.databinding.ActivitySignupBinding
import com.davidnuno.profilecreation.viewModel.SignUpViewModelFactory
import com.davidnuno.profilecreation.viewModel.UserRegisterViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var userViewModel: UserRegisterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        val binding: ActivitySignupBinding = DataBindingUtil.setContentView(
            this, R.layout.activity_signup
        )

        val application = requireNotNull(this).application
        val dao = UserDatabase.getInstance(application).userDao
        val repository = UserRepository(dao)
        val factory = SignUpViewModelFactory(repository, application)

        userViewModel = ViewModelProvider(this, factory)[UserRegisterViewModel::class.java]

        userViewModel.navigateActivity.observe(this, Observer {

            if (it != null) {
                userViewModel.finishedNavigating()
                val intent = Intent(this, ConfirmationActivity::class.java).apply {

                    putExtra("first_name", it.firstName)
                    putExtra("email", it.userName)
                    putExtra("website", it.website)
                }

                startActivity(intent)
            }
        })
        binding.userViewModel = userViewModel
        binding.lifecycleOwner = this
    }
}
package com.davidnuno.profilecreation

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_confirmation.*

class ConfirmationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)


        intent.getStringExtra("first_name")?.let {
            Log.v("ConfirmationActivity", "LOGGER: Users first name is not null")
            val greetingUser =
                getString(R.string.title_greeting_user) + intent.getStringExtra("first_name")

            tv_hello_user.text = greetingUser
            tv_name.text = intent.getStringExtra("first_name")

        } ?: run {

            Log.v("ConfirmationActivity", "LOGGER: Users first name is null")
            tv_hello_user.text = getString(R.string.title_greeting_no_name)
            tv_name.visibility = View.GONE
        }

        intent.getStringExtra("website")?.let {

            Log.v("ConfirmationActivity", "LOGGER: Website is not null")
            tv_website.text = intent.getStringExtra("website")
            tv_website.visibility = View.VISIBLE
        } ?: run {
            Log.v("ConfirmationActivity", "LOGGER: Website is null")
            tv_website.visibility = View.GONE
        }

        tv_email.text = intent.getStringExtra("email")
    }
}
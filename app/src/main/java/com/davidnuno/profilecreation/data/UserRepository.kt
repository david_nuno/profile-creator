package com.davidnuno.profilecreation.data

class UserRepository(private val dao: UserDao) {

    val users = dao.getAllUsers()

    suspend fun insert(user: UserEntity) {

        dao.insert(user)
    }

    suspend fun getEmail(email: String): UserEntity? {

        return dao.getEmail(email)
    }
}
package com.davidnuno.profilecreation.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users_table")
data class UserEntity(

    @PrimaryKey(autoGenerate = true)
    var userId: Int = 0,

    @ColumnInfo(name = "first_name")
    var firstName: String?,

    @ColumnInfo(name = "email")
    var userName: String,

    @ColumnInfo(name = "website")
    var website: String?,

    @ColumnInfo(name = "password_text")
    var passwrd: String
)
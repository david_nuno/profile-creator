package com.davidnuno.profilecreation.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDao {

    @Insert
    suspend fun insert(user: UserEntity)

    @Query("SELECT * FROM users_table ORDER BY userId")
    fun getAllUsers(): LiveData<List<UserEntity>>

    @Query("SELECT * FROM users_table WHERE email LIKE :email")
    suspend fun getEmail(email: String): UserEntity?
}